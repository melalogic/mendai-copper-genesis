# HOW TO DEPLOY

`sh ./_deploy.sh TARGET_S3_BUCKET CLOUDFRONT_DISTRIBUTION_ID AWS_PROFILE`

# HOW TO UPDATE BLOG POSTS
Blog posts can be updated two ways:
1. through forestry.io, which updates the `content` branch
2. directly through the repo, preferably via the `development` branch

# HOW TO UPDATE BLOG POST ICONS
1. always use an svg version of the logo. This requires, in many cases, fishing around online for a .svg version of the organization's logo. If that can't be found, try contacting the organization, or as a last resort, creating your own, or just not using any logo at all.
2. add the logo into an icomoon.io profile.
3. TO DO ...
4. TO DO ...