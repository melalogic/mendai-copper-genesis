var ai = ai || {}
ai.mend = ai.mend || {}
ai.mend.FileSorter = {
	init: () => {

		/*
			File Sorter
		*/
		
		const filesContainer = document.getElementById('files')
		if (filesContainer) {

			let sortDirection
			let lastSortedColumn
			const files = filesContainer.querySelectorAll('.file')
			const flattenedIndex = []
			files.forEach( file => {
				flattenedIndex.push({
					'year': file.getElementsByClassName('year')[0].dataset.date,
					'title': file.getElementsByClassName('title')[0].innerText.toLowerCase(),
					'format': file.getElementsByClassName('format')[0].innerText.toLowerCase(),
					'for': file.getElementsByClassName('for')[0].innerText.toLowerCase(),
					'element': file
				})
			})

<<<<<<< HEAD
			const sortFiles = (a, b, sortBy, direction) => {
				let result = 0
				switch (sortBy) {
					case 'year':
						if (direction === 'ASC') {
							if (a < b) {
								result = 1
							} else if (a > b) {
								result = -1
							}
							} else {
								if (a > b) {
									result = 1
								} else if (a < b) {
									result = -1
								}
							}
						break;
					default:
						if (direction === 'ASC') {
							if (a > b) {
								result = 1
							} else if (a < b) {
								result = -1
							}
						} else {
							if (a < b) {
								result = 1
							} else if (a > b) {
								result = -1
							}
						}
				}

=======
			const sortFiles = (a, b, direction) => {
				let result = 0
				if (direction === 'ASC') {
					if (a > b) {
						result = 1
					} else if (a < b) {
						result = -1
					}
				} else {
					if (a < b) {
						result = 1
					} else if (a > b) {
						result = -1
					}
				}
>>>>>>> 667c94f9cd76bbe57febcd6f1215e731cd19b83a
				return result
			}

			const buttons = document.querySelectorAll('#file-cabinet button')
			const caretUp = document.createElement('i')
			caretUp.classList.add('font-icon-caret-up')
			caretUp.setAttribute('role', 'icon')
			caretUp.setAttribute('aria-label', 'Sort Ascendingly')
			const caretDown = document.createElement('i')
			caretDown.classList.add('font-icon-caret-down')
			caretUp.setAttribute('role', 'icon')
			caretUp.setAttribute('aria-label', 'Sort Descendingly')

			// on click, sort files
			document.querySelectorAll('.sort-by').forEach( control => {
				const sortBy = control.classList[1]
				const button = control.querySelector('button')
				button.onclick = item => {

					let caret
					// if the sort direction is set to "ASC", then
						// set direction to "DESC"
						// set caret to up
					// else
						// set direction to "ASC"
						// set caret to down
					if (sortDirection === 'ASC') {
						sortDirection = 'DESC'
						caret = caretUp

					} else {
						sortDirection = 'ASC'
						caret = caretDown
					}

					//if the last sorted column does not match the current "sortBy" value, then
						// force the sort direction to "ASC"
						// set caret to down
					if (lastSortedColumn !== sortBy) {
						sortDirection = 'ASC'
						caret = caretDown
					}

					// clear any previously set carets and then set the current one
					caretUp.remove()
					caretDown.remove()
					button.appendChild(caret)
					
					// sort
<<<<<<< HEAD
					flattenedIndex.sort((a, b) => sortFiles(a[sortBy], b[sortBy], sortBy, sortDirection))
=======
					flattenedIndex.sort((a, b) => sortFiles(a[sortBy], b[sortBy], sortDirection))
>>>>>>> 667c94f9cd76bbe57febcd6f1215e731cd19b83a
					flattenedIndex.forEach( ({ element }) => {
						filesContainer.appendChild(element)
					})

					lastSortedColumn = sortBy
				}
			})
		}
	}
}
<<<<<<< HEAD
document.addEventListener('DOMContentLoaded', () => {
	ai.mend.FileSorter.init()
})
=======
ai.mend.FileSorter.init()
>>>>>>> 667c94f9cd76bbe57febcd6f1215e731cd19b83a
